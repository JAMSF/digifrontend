angular
.module('app')
.service('UserSrv', UserSrv)

UserSrv.$inject = ['$http','$q'];

function UserSrv($http,$q){
  /**
   * LOG USER
   */
  this.logUser = function (userCredentials){
    console.log("DEBUG 22", userCredentials);
    var deferred = $q.defer();
    $http({
      method : 'POST',
      url : "https://digitutorapi.azurewebsites.net/login",
      data: $.param(userCredentials),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    }).then(
      function mySuccess(response) {
        console.log("DEBUG",response.data);

        deferred.resolve(response.data);
      }, function myError(error) {
      console.log("DEBUG 1",error);
      deferred.reject(error);
    });

    return deferred.promise;
  };

  /*
  REGISTER USER
  */
  this.regUser = function (userData) {
      console.log("JSON", userData);
    var deferred = $q.defer();
    $http({
      method : "POST",
      url: "https://digitutorapi.azurewebsites.net/api/user/register",

      data: $.param(userData),
      headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
    }).then(function mySuccess(response) {
      console.log("DEBUGSucess",response);
      deferred.resolve(response);
    }, function myError(error) {
      console.log("DEBUGError",error);
      deferred.reject(error.data);
    });
    return deferred.promise;
  };

  /**/
  this.getMyUserinfo = function (idUser){
    var deferred = $q.defer();
    $http({
      method : "GET",
      url : "https://digitutorapi.azurewebsites.net/api/user/getInfoUser?idUser="+idUser,
      data: ""
    }).then(function mySuccess(response) {
      console.log("DEBUG: User info ", response.data);
      //console.log("DEBUG",response.data);
      /*response.data ={
        status: 100,
        rol: 1, // tutor
        id: "12345",
        name: "John Doe",
        since: "1288323623006",
        carne: "201236227",
        reputation: "33",
        description: "esta es una descripción",
        country: "Costa Rica",
        _college: {
          name: "Tecnológico de Costa Rica",
          country: "Cosa Rica",
          id:  "123"
        },
        email: "jnavcamacho@gmail.com",
        email2: "junavarro@estudiantec.cr",
        phone: "13456789",
        skills: [{
          category: "CAT 1",
          name: "SK 1",
          puntuation: 12
        },{
          category: "CAT 1",
          name: "SK 6",
          puntuation: 2
        },{
          category: "CAT 2",
          name: "SK 11",
          puntuation: 22
        }]
      };
      */
      deferred.resolve(response.data);
    }, function myError(error) {
      //console.log("DEBUG",error);
      deferred.reject(error.data);
    });
    return deferred.promise;
  }
};

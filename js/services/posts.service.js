angular
.module('app')
.service('PostsSrv', PostsSrv)

PostsSrv.$inject = ['$http','$q','toastr','$state'];

function PostsSrv($http,$q,toastr,$state){

  /*Get Colleges*/
  this.getPosts = function(userId){
    //console.log("DEBUG", userCredentials);
    var deferred = $q.defer();
    $http({
      method : "GET",
      url : "https://digitutorapi.azurewebsites.net/feed?IdUser="+userId
    }).then(function mySuccess(response) {
      var result =  JSON.parse(response.data);
      console.log("DEBUG", "FEED",result);
      deferred.resolve(result);
    }, function myError(error) {
      //console.log("DEBUG",error);
      deferred.reject(error.data);
    });
    return deferred.promise;
  }

  this.getMyPosts = function (userId){
    var deferred = $q.defer();
    $http({
      method : "GET",
      url : "https://digitutorapi.azurewebsites.net/myposts?IdUser="+userId
    }).then(function mySuccess(response) {
      var result =  JSON.parse(response.data);
      console.log("DEBUG",result);
      deferred.resolve(result);
    }, function myError(error) {
      //console.log("DEBUG",error);
      deferred.reject(error.data);
    });
    return deferred.promise;
  };
  /* get comment*/
  this.getComments = function(postId){
    //console.log("DEBUG", userCredentials);
    var deferred = $q.defer();
    $http({
      method : "GET",
      url : "https://digitutorapi.azurewebsites.net/comentarios?IdPost="+postId
    }).then(function mySuccess(response) {
      //console.log("DEBUG",response.data);
      console.log(response);
      /*response.data = [{
        user: {
          name: " Jonh Doe",
          userId: "12345"
        },
        date: new Date(),
        title: "Title 1",
        detail: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."

      },
      {
        user: {
          name: " Jonh Doe2",
          userId: "123457"
        },
        date: new Date(),
        title: "Title 1",
        detail: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."

      },{
        user: {
          name: " Jonh Doe 3",
          userId: "123457"
        },
        date: new Date(),
        title: "Title 1",
        detail: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."

      }];*/

      deferred.resolve(JSON.parse(response.data));
    }, function myError(error) {
      //console.log("DEBUG",error);
      deferred.reject(error.data);
    });
    return deferred.promise;
  };

  /*make a comment */
  this.deleteComment = function(messageId){
    console.log("Deleting message", messageId);
    var deferred = $q.defer();
    var url =  "https://digitutorapi.azurewebsites.net/delComment";
    var post = {CommentId: messageId};
    $http(
      {
        method : "POST",
        url : url,
        data : $.param(post),
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
      }).then(function (response){
        toastr.success('Comentario eliminado',response);
        deferred.resolve(response);
        $state.reload();
      },function(error){
        deferred.reject(error);
        console.log("ERROR ",error);
        toastr.error('Comentario no eliminado',error);
      });
    return deferred.promise;
  };
  /*check if the user is subribed to some user*/
  this.checkSubscription = function (user,suscriber){
    console.log("DEBUG", "check subscription", user, suscriber);
    var deferred = $q.defer();
    var url =  "https://digitutorapi.azurewebsites.net/api/user/getcheckSubscription?IdUser="+suscriber+ "&IdFollowed=" +user ;
    $http(
      {
        method : "GET",
        url : url
      }).then(function (response){
        console.log("DEBUG", "check subscription data ", response);
        deferred.resolve(response.data);
      },function(error){
        console.log("DEBUG", "check subscription error ", error);
        deferred.reject(error);
      });
    return deferred.promise;
  };

  /*signin tutorial*/
  this.subscription = function(user,suscriber){

    var deferred = $q.defer();
    var url =  "https://digitutorapi.azurewebsites.net/api/user/subscribe"
    var post = {IdUser: suscriber,IdFollowed: user};
    $http(
      {
        method : "POST",
        url : url,
        data: $.param(post),
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
      }).then(function (response){
        deferred.resolve(response.data);
        console.log("DEBUG","Ok subscribe ",response);
        toastr.success('Usuario inscrita',response);
      },function(error){
        console.log("DEBUG","ERROR subscribe ",error);
        deferred.reject(error);
        toastr.error('Usuario no inscrita',error);
      });
    return deferred.promise;
  };

  /*signout tutorial*/
  this.unsubscription = function(postId,userId){
    console.log(postId,userId);
    var deferred = $q.defer();
    var url =  "http://digitutorapi.azurewebsites.net/post/unsubscribe/" + postId;
    var url2 =  "https://jsonplaceholder.typicode.com/posts";
    $http(
      {
        method : "POST",
        url : url2,
        /*data : $.param({'userId': userId})*/
        data :{'userId': userId}
      }).then(function (response){
        var res = {
          request_unsubscribtion: true
        };
        deferred.resolve(res);
      },function(error){
        deferred.reject(error);
        console.log("ERROR ",error);
      });
    return deferred.promise;
  };

  /*create comment*/
  this.createComment = function(message){
    console.log("Posting message", message);
    var deferred = $q.defer();
    var url =  "https://digitutorapi.azurewebsites.net/postComment";
    var msj = {
      IdUser : message.userId,
      IdPost: message.postId,
      CommentDetail: message.message
    }
    $http(
      {
        method : "POST",
        url : url,
        data: $.param(msj),
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
      }).then(function (response){
        console.log("DEBUG", "MSJ",response);
        toastr.success('Comentario creada',response);
        deferred.resolve(response.data);
        $state.reload();
      },function(error){
        deferred.reject(error);
        toastr.error('Comentario NO creada',error);
        console.log("ERROR ",error);
      });
    return deferred.promise;
  };

  /*Sign in for tutorial*/
  this.sign = function (UserId,PubIdd) {
    console.log("DEBUG", "Sign in POST user, post", UserId, PubIdd);
    var deferred = $q.defer();
    var url =  "https://digitutorapi.azurewebsites.net/postInscribe";
    var postObj = {
      UserId: UserId,
      PubId: PubIdd
    };
    $http(
      {
        method : "POST",
        url : url,
        data: $.param(postObj),
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
      }).then(function (response){
        console.log("DEBUG", "Like", response);
        deferred.resolve(response.data);
        if( response.data.status == 200){
            toastr.success('Usuario inscrito a tutoría',response.data);
        }
        else{
          toastr.error('Usuario no inscrito a tutoría',response.data);
        }

      },function(error){
        console.log("DEBUG", " Error Like", error);
        deferred.resolve(error);
        toastr.error('Usuario no inscrito a tutoría',error);
      });
    return deferred.promise;
  };
  /* Sign out for tutorial*/
  this.signout = function (UserId,PubId) {
    console.log("DEBUG", "Sign in POST user, post", UserId, PubId);
    var deferred = $q.defer();
    var url =  "https://digitutorapi.azurewebsites.net/delInscribe";
    var postObj = {
      UserId: UserId,
      PubId: PubId
    };
    $http(
      {
        method : "POST",
        url : url,
        data: $.param(postObj),
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
      }).then(function (response){
        console.log("DEBUG", "Like", response);
        deferred.resolve(response.data);
        if( response.data.status == 200){
            toastr.success('Usuario no inscrito a tutoría',response.data);
        }
        else{
          toastr.error('Operación no exitosa',response.data);
        }

      },function(error){
        console.log("DEBUG", " Error Like", error);
        deferred.resolve(error);
        toastr.error('Operación no exitosa',error);
      });
    return deferred.promise;
  };
  /*Check if an user is already subscribed to some tutorial*/
  this.checkTutorialSubscription = function (UserId,PubId){
    var deferred = $q.defer();
    var url =  "https://digitutorapi.azurewebsites.net/getCheckInscribe?UserId="+ UserId +"&PubId="+PubId;
    $http(
      {
        method : "GET",
        url : url
      }).then(function (response){
        console.log("DEBUG", "Like", response);
        deferred.resolve(response.data);

      },function(error){
        console.log("DEBUG", " Error Like", error);
        deferred.resolve(error);
      });
    return deferred.promise;
  };

  /*create post*/
  this.creatPost = function (post){
    var deferred = $q.defer();
    var url =  "http://digitutorapi.azurewebsites.net/comment/";
    var url2 =  "https://digitutorapi.azurewebsites.net/insertpublicacion";
    var url3 = "https://172.19.53.150:65137/insertpublicacion";
    console.log("DEBUG POST" ,post);
    $http(
      {
        method : "POST",
        url : url2,
        data: $.param(post),
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
      }).then(function (response){
        console.log("DEBUG", response);
        var res = {
          request_create_message: true
        };
        deferred.resolve(res);
      },function(error){
        deferred.reject(error);
        console.log("ERROR ",error);
      });
    return deferred.promise;
  };

  /*Like a post*/
  this.likeThisPost = function (userId,postId,like){
    console.log("DEBUG", "LIKE POST user, post", userId, postId);
    var deferred = $q.defer();
    var url =  "https://digitutorapi.azurewebsites.net/postLike";
    var postObj = {
      IdUsuario: userId,
      IdPublicacion: postId,
      Like : like
    };
    $http(
      {
        method : "POST",
        url : url,
        data: $.param(postObj),
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
      }).then(function (response){
        console.log("DEBUG", "Like", response);
        deferred.resolve(response.data);
      },function(error){
        console.log("DEBUG", " Error Like", error);
        deferred.resolve(error);
      });
    return deferred.promise;
  }
  /*Dislike a post*/
  this.dislikeThisPost = function (userId,postId){
    console.log("DEBUG", "DISLIKE POST user, post", userId, postId);
    var deferred = $q.defer();
    var url =  "http://digitutorapi.azurewebsites.net/comment/";
    var postObj = {
      UserId: userId,
      PostId: postId
    };
    $http(
      {
        method : "POST",
        url : url,
        data: $.param(postObj),
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
      }).then(function (response){
        console.log("DEBUG", "DISLike", response);
        deferred.resolve(response.data);
      },function(error){
        console.log("DEBUG", " Error DISLike", error);
        deferred.resolve(error);
      });
    return deferred.promise;

  }
  /*Support an skill*/
  this.support = function (skill,userId){
    //console.log("DEBUG", "Skill", skill, userId);
    console.log("DEBUG", "Sign in POST user, post", userId, skill);
    var deferred = $q.defer();
    var url =  "https://digitutorapi.azurewebsites.net/api/user/postSupport";
    var postObj = {
      UserId: userId,
      Key: skill.key
    };
    $http(
      {
        method : "POST",
        url : url,
        data: $.param(postObj),
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
      }).then(function (response){
        console.log("DEBUG", "Like", response);
        deferred.resolve(response.data);
        if (response.data.status == 200){
          toastr.success('Apoyo exitoso',response.data);
        }else {
          toastr.warning('Apoyo no efectuado',response.data);
        }


      },function(error){
        console.log("DEBUG", " Error Like", error);
        deferred.resolve(error);
        toastr.error('Error apoyando',response.data);
      });
    return deferred.promise;
  };

  /*get detail files*/
  this.getFileDetail = function(idPost){
    console.log("DEBUG", "Get Files");
    var deferred = $q.defer();
    var url =  "https://digitutorapi.azurewebsites.net/files/listfiles?idPost="+idPost;
    $http(
      {
        method : "GET",
        url : url
      }).then(function (response){
        console.log("DEBUG", "Files", response.data);
        deferred.resolve(response.data);
        //toastr.success('Files sucessful','response.data');

      },function(error){
        console.log("DEBUG", " Error Like", error);
        deferred.resolve(error);
        //toastr.error('Error files','response.data');
      });
    return deferred.promise;
  };

};

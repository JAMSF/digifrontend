angular
.module('app')
.service('DigiStorage', DigiStorage)

DigiStorage.$inject = ['$http','$q','$window'];

function DigiStorage($http,$q,$window){
  /* log out user : clear session storage */
  this.clearSessionData =function(){
    $window.sessionStorage. clear();
  }
  /*Register an user */
  this.saveLoggedUserInfo =function(credentials){
    /*save data for tutor or admin*/
    $window.sessionStorage.setItem('userId',credentials.id);
    $window.sessionStorage.setItem('rol',credentials.rol);
  }
  /* get the last sesión*/
    this.getLastUserLogged = function (){
      return $window.sessionStorage.userId;
    };

  /* check permissions of user */
  this.chekPermissions = function(rolType){
    var session_rol =$window.sessionStorage.rol;
    console.log("DEBUG: session", session_rol,rolType, session_rol == rolType);
    return session_rol == rolType;
  };
};

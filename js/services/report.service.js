﻿angular
.module('app')
.service('ReportSrv', ReportSrv)

ReportSrv.$inject = ['$http', '$q'];

function ReportSrv($http, $q) {
    /*
    Create Report
    */
    this.createReport = function (reportData) {
        console.log("JSON", reportData);
        var deferred = $q.defer();
        $http({
            method: "POST",
            url: "https://digitutorapi.azurewebsites.net/api/admin/report",
            data: $.param(reportData),
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        }).then(function mySuccess(response) {
            console.log("DEBUGSucess", response.data);
            
            deferred.resolve(response);
        }, function myError(error) {
            console.log("DEBUGError", error);
            deferred.reject(error.data);
        });
        return deferred.promise;
    };
};

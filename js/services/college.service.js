angular
.module('app')
.service('CollegeSrv', CollegeSrv)

CollegeSrv.$inject = ['$http','$q'];

function CollegeSrv($http,$q){

  /*Get Colleges
  */
  this.getColleges =function(){
    //console.log("DEBUG", userCredentials);
    var deferred = $q.defer();
    $http({
      method : "GET",
      url : "https://digitutorapi.azurewebsites.net/universidades"
    }).then(function mySuccess(response) {
      //console.log("DEBUG",response.data);
      deferred.resolve(response.data);
    }, function myError(error) {
      //console.log("DEBUG",error);
      deferred.reject(error.data);
    });
    return deferred.promise;
  }
};

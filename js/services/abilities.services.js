﻿angular
.module('app')
.service('AbilitieSrv', AbilitieSrv)

AbilitieSrv.$inject = ['$http', '$q'];

function AbilitieSrv($http, $q) {

    /*Get Abilities
    */
    this.getAbilities = function () {
        //console.log("DEBUG", userCredentials);
        var deferred = $q.defer();
        $http({
            method: "GET",
            url: "https://digitutorapi.azurewebsites.net/habilidades"
        }).then(function mySuccess(response) {
            //console.log("DEBUG",response.data);
            deferred.resolve(response.data);
        }, function myError(error) {
            //console.log("DEBUG",error);
            deferred.reject(error.data);
        });
        return deferred.promise;
    }

    /*Get getCategoriesGroup
   */
    this.getAbilitiesbyGroup = function () {
        //console.log("DEBUG", userCredentials);
        var deferred = $q.defer();
        $http({
            method: "GET",
            url: "https://digitutorapi.azurewebsites.net/getCategoriesGroup"
        }).then(function mySuccess(response) {
            //console.log("DEBUG",response.data);
            deferred.resolve(response.data);
        }, function myError(error) {
            //console.log("DEBUG",error);
            deferred.reject(error.data);
        });
        return deferred.promise;
    }



};
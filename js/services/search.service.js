﻿angular
.module('app')
.service('SearchSrv', SearchSrv)

SearchSrv.$inject = ['$http', '$q'];

function SearchSrv($http, $q) {
    /*
    Create Search
    */
    this.searchUser = function (searchData) {
        console.log("Service", searchData);
        var deferred = $q.defer();
        $http({
            method: "POST",
            url: "https://digitutorapi.azurewebsites.net/api/user/search",
            data: $.param(searchData),

            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        }).then(function mySuccess(response) {
            console.log("DEBUGSucess", response.data);
            deferred.resolve(response);
        }, function myError(error) {
            console.log("DEBUGError", error);
            deferred.reject(error.data);
        });
        return deferred.promise;
    };
};

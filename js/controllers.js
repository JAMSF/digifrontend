// controller.js
angular.module('app')
.controller('testCtrl', function ($scope,$http,$q){
	
	$http(
		{
		method: 'GET',
		url: 'https://jsonplaceholder.typicode.com/posts'}
	).then(
		function(response) {
          $scope.data = response.data;}, 
        function(error) {
          alert(error.data);
      	});
});

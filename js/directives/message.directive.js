(function(){
  /**
   * @memberof app
   * @ngdoc directive
   * @name message
   */
  angular
    .module('app')
    .directive('message',message);
    message.$inject = ['PostsSrv','toastr'];
  function message(PostsSrv,toastr){
    return {
      restrict: 'E',
      scope: {
        data : '=data',
        publisher : '=publisher'
      },
      link: function(scope, element, attrs) {
        //console.log("DEBUG","MESSAGE",scope.data);
        /*Delete message*/
        scope.deleteMSJ = function () {
          console.log("DEBUG", "delete message",scope.data);
          PostsSrv.deleteComment(scope.data.IdComentario).then(
            function (response){

            },
            function (error){

            }
          );
        };
      },
      templateUrl : 'views/directives-views/message.html'}
  };

})();

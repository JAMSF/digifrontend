(function(){
  /**
  * @memberof app
  * @ngdoc directive
  * @name post
  */
  angular
  .module('app')
  .directive('digipost',digipost);
  digipost.$inject = ['PostsSrv','toastr'];
  function digipost(PostsSrv,toastr){
    return {
      restrict: 'E',
      scope: {
        post : '=data',
        user : '=user',
        myposts : '=myposts'

      },
      link: function(scope, element, attrs) {
        //console.log("DEBUG POST USER:",scope.user);
        console.log("DEBUG POST:",scope.post);


        /*Check tutorial description*/
        PostsSrv.checkTutorialSubscription(scope.user.id,scope.post.idTutorial).then(
          function (response){
            console.log("DEBUG", "check subscription", response);
            scope.alreadySubscribed = response == 1 ? true : false;
          },
          function (error){

          }
        );

        scope.showComments = false;
        /**
        * subscribe / unsubscribe
        */
        scope.signin = function (value){
          if(value){
            PostsSrv.sign(scope.user.id,scope.post.idTutorial).then(
              function (response){
                if (response.status == 200){
                  //console.log("DEBUG ","sign in",response);
                  scope.post.signedIn = value;
                }else {
                  //console.log("DEBUG ","error sign in",response);
                }
              },
              function (error){
                //console.log("Error ","subscription",error);
              }
            );
          }
          else{
            PostsSrv.signout(scope.user.id,scope.post.idTutorial).then(
              function (response){
                if (response.status == 200){
                  //console.log("DEBUG ","sign in",response);
                  scope.post.signedIn = value;
                }else {
                  //console.log("DEBUG ","error sign in",response);
                }
              },
              function (error){
                //console.log("Error ","subscription",error);
              }
            );
            console.log("DEBUG", scope.post.signedIn);
          };
        }
        /**
        * get the comments related to some post
        */
        scope.getComments = function (post){
          console.log("DEBUG 77",post);
          scope.showComments = !scope.showComments;
          PostsSrv.getComments(post.idTutorial).then(
            function (response){
              post.comments = response;
              console.log("DEBUG 4", post.comments);
            },
            function (error){
              console.log("DEBUG", "Error", error);
            }
          );
        };


        /**
        * send message
        */
        scope.sendMessage = function (new_message) {
          var type_of_post = "academic"; // academic content default
          if (scope.post.tutorial){
            type_of_post = "tutorial";
          }
          var message = {
            usernmaName: scope.user.name,
            postype: type_of_post,
            userId: scope.user.id,
            postId: scope.post.idTutorial,
            message: new_message,
            date: new Date()
          };
          console.log("DEBUG 6 ",scope.post);
          PostsSrv.createComment(message).then(

            function (response) {
              if (response.status == 200){
                var response3 = {
                  Nombre : message.usernmaName,
                  Fecha: message.date,
                  title: "Comentario",
                  DetalleComentario: message.message,
                  IdUsuario : message.userId,
                  postId : message.postId

                };
                //insert at first
                scope.post.comments.unshift(response3);
                console.log("DEBUG", "CREATION MSG", response3);

              }
              else if (response == 500) {
                console.log("DEBUG", " ERROR CREATION MSG", response);
              }

            },
            function (error) {
              console.log("DEBUG ERROR", "DIRECTIVE", error);

            }

          );
        };
        /* To like a post
        */
        scope.likeThisPost = function (like){
          PostsSrv.likeThisPost(scope.user.id,scope.post.idTutorial,like).then(
            function (response){
              console.log("DEBUG", "LIKE/USER/POST", like,scope.user.id,scope.post.idTutorial,response);
            },
            function (error){
              console.log("DEBUG Error", "LIKE/USER/POST", like,scope.user.id,scope.post.idTutorial,error);
            }
          );
        };
        /* To dislike a post
        */
        scope.dislikePost = function () {
          PostsSrv.dislikeThisPost(scope.user.id,scope.post.idTutorial).then(
            function (response){

            },
            function (error){

            }
          );
        };

        /*get files*/
        PostsSrv.getFileDetail(scope.post.ContainerId).then(
          function (response){
            console.log("DEBUG", "BLOB2",scope.post);
            scope.post.docs = response;
            //if (scope.post.docs.PDF){
              scope.pdf_url = "https://digitutorapi.azurewebsites.net/files/download?idPost="+scope.post.ContainerId+"&name="+scope.post.docs.PDF.namefile;
            //}
            //if(scope.post.docs.Word){
              scope.word_url = "https://digitutorapi.azurewebsites.net/files/download?idPost="+scope.post.ContainerId+"&name="+scope.post.docs.Word.namefile;

            //}
          //  if(scope.post.docs.Excel){
              scope.xls_url = "https://digitutorapi.azurewebsites.net/files/download?idPost="+scope.post.ContainerId+"&name="+scope.post.docs.Excel.namefile;

            //}
            //if(scope.post.docs.PPT){
              scope.ppt_url = "https://digitutorapi.azurewebsites.net/files/download?idPost="+scope.post.ContainerId+"&name="+scope.post.docs.PPT.namefile;

            //}
          },
          function (error){
            console.log("DEBUG", "ERROR FILE");
          }
        );
      },
      templateUrl : 'views/directives-views/post.card.html'}
    };

  })();

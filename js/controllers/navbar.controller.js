angular
.module('app')
.controller('navBarCtrl', navBarCtrl)
navBarCtrl.$inject = ['$scope','UserSrv','$state','$rootScope','$stateParams','PostsSrv','$state','DigiStorage','$filter'];

function navBarCtrl ($scope,UserSrv,$state,$rootScope,$stateParams,PostsSrv,$state,DigiStorage,$filter) {
  $scope.menuForTutor = DigiStorage.chekPermissions(2);
  $scope.userId = DigiStorage.getLastUserLogged();
  /*Go to my  home*/
  $scope.goToHome = function (){
    console.log("DEBUG", "HOME");
    $state.go("app.main",{'tutorId':$scope.userId });
  };
  /*Go to my pubs*/
  $scope.goToMyPubs = function (){
    console.log("DEBUG", "PUBS");
    $state.go("app.myposts",{'userId':$scope.userId });

  };
  /* Go to search */
  $scope.goToSearch = function (){
    console.log("DEBUG", "SEARCH");
    $state.go("app.search",{'userId':$scope.userId });
  };
};

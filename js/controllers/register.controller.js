angular
.module('app')
.controller('RegisterCtlr', RegisterCtlr)
RegisterCtlr.$inject = ['$scope', 'UserSrv', 'CollegeSrv','AbilitieSrv', '$state'];

function RegisterCtlr($scope, UserSrv, CollegeSrv, AbilitieSrv, $state) {

    //variable to store the user data
    //$scope.password = "";
    //$scope.nickname = "";
    $scope.user = {
        name: "",
        lastname: "",
        country: "",
        college: "",
        email1: "",
        email2: "",
        id: "",
        idrol: "",
        pass: "",
        password: "",
        phone: "",
        cellphone: "",
        description: "",
        photo: "",
        skills: []
    };
    $scope.collegeId = null;

    $scope.colleges = [];
    $scope.paises = ["Costa Rica", "Estados Unidos", "España", "Alemania", "Argentina", "Brasil", "Canadá", "Chile", "Colombia", "Guatemala", "México", "Panamá"];

    //get the colleges
    CollegeSrv.getColleges().then(
      function (data) {
          //console.log("DEBUG", data);
          $scope.colleges = angular.fromJson(data);
          //console.log("DEBUG", $scope.colleges);
      },
      function (error) {
          console.log("DEBUG", error);
      }
    );

    /**
      Register's
       User .
    **/
    $scope.skillcheckbox = {
        estado: false,
        id: null,
    };
            
    
    $scope.addSkill = function (skill) {
        console.log("Debug List of Skill:", skill.id);
        //$scope.skills.push($scope.skill.id);
    }
  

    $scope.registerUser = function () {

        if ($scope.pass == $scope.password) {

            if ($scope.checkAdminUser) {
                $scope.errorRegister = false;
                $scope.user = ({
                    Name: $scope.name,
                    LName1: $scope.lastname,
                    LName2: $scope.lastname,
                    Country: $scope.paiss,
                    IdUniversity: "N/A",
                    IdRole: 1,
                    PEmail: $scope.email1,
                    PEmail2: "N/A",
                    PCarnet: $scope.id,
                    Password: $scope.pass,
                    //contrasena: $scope.password,
                    Phone: "N/A",
                    Movil: "N/A",
                    Description: "N/A",
                    Photo: "N/A",
                });
            }
            else {
                $scope.errorRegister = false;
                $scope.user = ({
                    Name: $scope.name,
                    LName1: $scope.lastname,
                    LName2: $scope.lastname,
                    Country: $scope.paiss,
                    IdUniversity: $scope.college.IdUniversidad,
                    IdRole: 2,
                    PEmail: $scope.email1,
                    PEmail2: $scope.email2,
                    PCarnet: $scope.id,
                    Password: $scope.pass,
                    //contrasena: $scope.password,
                    Phone: $scope.phone,
                    Movil: $scope.cellphone,
                    Description: $scope.description,
                    Photo: $scope.photo,
                });
            };
        }
        else {
            $scope.errorRegister = true;
            $scope.user = {
                name: "",
                lastname: "",
                country: "",
                college: "",
                email1: "",
                email2: "",
                id: "",
                idrol: "",
                pass: "",
                password: "",
                phone: "",
                cellphone: "",
                description: "",
                photo: "",
                skills: []
            };
        }
       
        UserSrv.regUser($scope.user).then(
          function (response) {
              console.log("DEBUG cTRL", response);
              if (response.status == 200) {
                  $state.go('appSimple.login');
              } else {
                  $scope.errorRegister = true;
                  $scope.user = {
                      name: "",
                      lastname: "",
                      country: "",
                      college: "",
                      email1: "",
                      email2: "",
                      id: "",
                      idrol: "",
                      pass: "",
                      password: "",
                      phone: "",
                      cellphone: "",
                      description: "",
                      photo: "",
                      skills: []
                  };
              }
          },
          function (error) {
              console.log("DEBUG", error);
              $scope.errorRegister = true;
              $scope.user = {
                  name: "",
                  lastname: "",
                  country: "",
                  college: "",
                  email1: "",
                  email2: "",
                  id: "",
                  idrol: "",
                  pass: "",
                  password: "",
                  phone: "",
                  cellphone: "",
                  description: "",
                  photo: "",
                  skills: []
              };
          }
    );
    };
    AbilitieSrv.getAbilitiesbyGroup().then(
      function (data) {
          console.log("DEBUG", data);
          $scope.categories = JSON.parse(data);
          console.log("DEBUG", data);

      },
      function (error) {
          console.log("DEBUG", error);
      }
    );

  


};

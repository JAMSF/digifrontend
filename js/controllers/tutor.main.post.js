angular
.module('app')
.controller('TutorMainPost', TutorMainPost)
TutorMainPost.$inject = ['$scope','UserSrv','$state','$rootScope','$stateParams','PostsSrv','$state','DigiStorage','$filter','$http'];

function TutorMainPost ($scope,UserSrv,$state,$rootScope,$stateParams,PostsSrv,$state,DigiStorage,$filter,$http) {
  $scope.generateNumber = function(max,min) {
    return Math.floor(Math.random()*(max-min+1)+min);
  }
  $scope.content_post = {
    Title:"Título Quemado2222",
    Description:"Descripcción Quemada2",
    Course:"Curso quemado",
    StartDate:"2017-11-09 10:50:00.00",
    Site:"Lugar quemado2",
    Payment:1,
    Virtual:1,
    ContentType:1,
    IdUser:DigiStorage.getLastUserLogged(),
    UniversityId:1,
    ContainerId:$scope.generateNumber(10000000,1),
    YouTube : 'R6SstBoXjKc',
    GitHub : 'https://github.com/github/scientist'
  };
  $scope.content_type = "";
  $scope.dateToshow = {
    date : ""
  };
  $scope.dateToshow.date =  $filter('date')($scope.content_post.StartDate, "short");
  /**Validate rol just tutors can see this view **/
  if(!DigiStorage.chekPermissions(2)){
    console.log("DEBUG: ","Not authorized");
    $state.go("appSimple.404");
  }
  /*set the content type*/
  $scope.setContentType = function (type){
    $scope.content_type = type;
  };


  /**
  * Fetch data from server about the user info.
  */
  UserSrv.getMyUserinfo($stateParams.tutorId).then(
    function (response){
      console.log("DEBUG", "USER", response);
      $scope.user = response;
    },
    function (error){
      $state.go("appSimple.login");
      console.log("DEBUG", "ERROR", error);
    }
  );

  /**
  * Fetch the post from server.
  */
  PostsSrv.getPosts($stateParams.tutorId).then(
    function(response){
      $scope.posts = response;
    },
    function(error){
      //$state.go("appSimple.500");
      console.log("DEBUG", "ERROR", error);
    }
  );

  /*Variables to store the type of the content*/
  $scope.create_post = function (){

    //console.log("DEBUG: ", " POST", JSON.stringify($scope.content_post ));
    //set content type :
    $scope.content_post.ContentType = "tutorial" == $scope.content_type ? 1 : 0;
    //set user id of the content
    $scope.content_post.IdUser = DigiStorage.getLastUserLogged();
    //set uniervsity
    //$scope.content_post.UniversityId = $scope.user.college.id;
    $scope.content_post.UniversityId = $scope.user._college.id;
    //set the container id
    $scope.content_post.ContainerId =$scope.generateNumber(10000000,1);
    //set the date:
    $scope.content_post.StartDate = moment($scope.content_post.StartDate).format('YYYY-MM-DD h:mm:ss.00');
    //set number payment
    $scope.content_post.Payment = $scope.content_post.Payment ? 1 : 0;
    //set  number virtual
    $scope.content_post.Virtual = $scope.content_post.Virtual ? 1 : 0;
    //set number content_type
    $scope.content_post.ContentType = $scope.content_post.ContentType ? 1 : 0;
    //console.log("DEBUG: ", " POST", JSON.stringify($scope.content_post ));
    //set youtube
    //$scope.content_post.YouTube = 'R6SstBoXjKc';
    //set git hub
    $scope.content_post.GitHub = 'https://github.com/github/scientist';
    //send the file
    // send word
    // send pdf
    // send ppt
    // send xls
    $('#myModal').modal('hide');
    //create ppost usgin service
    PostsSrv.creatPost($scope.content_post).then(
      function (response) {
        console.log("Debug posting",JSON.stringify(response));
        $scope.uploadFiles();
        $scope.content_post = {};
      },
      function (error) {
        console.log("Debug posting",error);
        $scope.content_post = {};
      }
    );
  };

  /*Load files*/
  var formdata = new FormData();
  $scope.getTheFiles = function ($files) {

      angular.forEach($files, function (value, key) {
          formdata.append(key, value);
      });
  };
  // NOW UPLOAD THE FILES.
  $scope.uploadFiles = function () {
      var request = {
          method: 'POST',
          url: 'https://digitutorapi.azurewebsites.net/files/upload?idContainer='+$scope.content_post.ContainerId,
          data: formdata,
          headers: {
              'Content-Type': undefined
          }
      };

      // SEND THE FILES.
      $http(request).then(
        function (d) {alert(d);},function (error) { });



}

}

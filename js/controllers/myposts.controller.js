angular
.module('app')
.controller('MyPostsCtrl', MyPostsCtrl)
MyPostsCtrl.$inject = ['$scope','UserSrv','$state','$rootScope','DigiStorage','$filter','$stateParams','PostsSrv'];

function MyPostsCtrl ($scope,UserSrv,$state,$rootScope,DigiStorage,$filter,$stateParams,PostsSrv) {
  $scope.content_post = {
    Title:"Título Quemado2222",
    Description:"Descripcción Quemada2",
    Course:"Curso quemado",
    StartDate:"2017-11-09 10:50:00.00",
    Site:"Lugar quemado2",
    Payment:1,
    Virtual:1,
    ContentType:1,
    IdUser:DigiStorage.getLastUserLogged(),
    UniversityId:1,
    ContainerId:12
  };
  console.log("DEBUG","MyPosts");
  $scope.content_type = "";
  $scope.dateToshow = {
    date : ""
  };
  $scope.dateToshow.date =  $filter('date')($scope.content_post.StartDate, "short");
  /**Validate rol just tutors can see this view **/
  if(!DigiStorage.chekPermissions(2)){
    console.log("DEBUG: ","Not authorized");
    $state.go("appSimple.404");
  }
  /*set the content type*/
  $scope.setContentType = function (type){
    $scope.content_type = type;
  };


  /**
   * Fetch data from server about the user info.
   */
  UserSrv.getMyUserinfo($stateParams.userId).then(
    function (response){
      console.log("DEBUG", "USER", response);
      $scope.user = response;
    },
    function (error){
      $state.go("appSimple.login");
      console.log("DEBUG", "ERROR", error);
    }
  );

  /**
   * Fetch the my posts from server.
   */
  PostsSrv.getMyPosts($stateParams.userId).then(
    function(response){
      $scope.posts = response;
    },
    function(error){
      //$state.go("appSimple.500");
      console.log("DEBUG", "ERROR", error);
    }
  );

  /*Variables to store the type of the content*/
  $scope.create_post = function (){

    //console.log("DEBUG: ", " POST", JSON.stringify($scope.content_post ));
    //set content type :
    $scope.content_post.ContentType = "tutorial" == $scope.content_type ? 1 : 0;
    //set user id of the content
    $scope.content_post.IdUser = DigiStorage.getLastUserLogged();
    //set uniervsity
    //$scope.content_post.UniversityId = $scope.user.college.id;
    $scope.content_post.UniversityId = $scope.user._college.id;
    //set the container id
    $scope.content_post.ContainerId = 12;
    //set the date:
    $scope.content_post.StartDate = moment($scope.content_post.StartDate).format('YYYY-MM-DD h:mm:ss.00');
    //set number payment
    $scope.content_post.Payment = $scope.content_post.Payment ? 1 : 0;
    //set  number virtual
    $scope.content_post.Virtual = $scope.content_post.Virtual ? 1 : 0;
    //set number content_type
    $scope.content_post.ContentType = $scope.content_post.ContentType ? 1 : 0;
    //console.log("DEBUG: ", " POST", JSON.stringify($scope.content_post ));
  $('#myModal').modal('hide');
    //create ppost usgin service
    PostsSrv.creatPost($scope.content_post).then(
      function (response) {
        console.log("Debug posting",JSON.stringify(response));
        $scope.content_post = {};
      },
      function (error) {
        console.log("Debug posting",error);
        $scope.content_post = {};
      }
    );
  };
};

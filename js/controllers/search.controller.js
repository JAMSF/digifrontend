﻿angular
.module('app')
.controller('SearchCtrl', SearchCtrl)
SearchCtrl.$inject = ['$scope', 'CollegeSrv', 'AbilitieSrv', "SearchSrv", '$state','DigiStorage'];

function SearchCtrl($scope, CollegeSrv, AbilitieSrv, SearchSrv, $state,DigiStorage) {
  /**Validate rol just tutors can see this view **/
  if(!DigiStorage.chekPermissions(2)){
    console.log("DEBUG: ","Not authorized");
    $state.go("appSimple.404");
  }
    //variable to store the search data


    $scope.collegeId = null;
    $scope.colleges = [];
       //get the colleges
    CollegeSrv.getColleges().then(
      function (data) {
          //console.log("DEBUG", data);
          $scope.colleges = angular.fromJson(data);
          //console.log("DEBUG", $scope.colleges);
      },
      function (error) {
          console.log("DEBUG", error);
      }
    );

    $scope.abilityId1 = null;
    $scope.abilityId2 = null;
    $scope.abilityId3 = null;
    $scope.abilities = [];


    AbilitieSrv.getAbilities().then(
      function (data) {
          //console.log("DEBUG", data);
          $scope.abilities = angular.fromJson(data);
          //console.log("DEBUG", $scope.colleges);
      },
      function (error) {
          console.log("DEBUG", error);
      }
    );



    /**
      Criteria for a search .
    **/
    $scope.skill1 = null;
    $scope.skill2 = null;
    $scope.skill3 = null;

    $scope.numCarnet = null;
    $scope.userName = null;




    $scope.search = {
        college: "",
        Name: "",
        Carnet: "",
        skills: []
    };

    $scope.searchUser = function () {
        if ($scope.carnet == null) {
            $scope.carnet = "N/A";
        }
        if ($scope.name == null) {
            $scope.name = "N/A";
        }
       
        if (typeof $scope.abilityId1 === "undefined") {
            $scope.skill1 = -1;
        }
        else {
            $scope.skill1 = $scope.abilityId1.IdHabilidad;
        }
        if (typeof $scope.abilityId2 === "undefined") {
            $scope.skill2 = -1;
        }
        else {
            $scope.skill2 = $scope.abilityId2.IdHabilidad;
        }
        if (typeof $scope.abilityId3 === "undefined") {
            $scope.skill3 = -1;
        }
        else {
            $scope.skill3 = $scope.abilityId3.IdHabilidad;
        }

        if ($scope.universidad == null) {
            $scope.search = ({
                collegeId: -1,
                Name: $scope.name,
                Lastname1: "N/A",
                Lastname2: "N/A",
                Carnet: $scope.carnet,
                skillsId: [$scope.skill1, $scope.skill2, $scope.skill3],
            });
        }
        else {
            $scope.search = ({
                collegeId: $scope.universidad.IdUniversidad,
                Name: $scope.name,
                Lastname1: "N/A",
                Lastname2: "N/A",
                Carnet: $scope.carnet,
                skillsId: [$scope.skill1, $scope.skill2, $scope.skill3],
            });
        }


        $scope.errorRegister = false;
        SearchSrv.searchUser($scope.search).then(
          function (response) {
              console.log("DEBUG JS", response);
              if (response.status == 200) {
                  $scope.listaTutores = JSON.parse(response.data); // get data from json
                  console.log("DEBUG tutor", $scope.listaTutores);
              } else {
                  $scope.errorRegister = true;
                  $scope.search = {
                      collegeId: $scope.universidad.IdUniversidad,
                      Name: "",
                      Lastname1: "",
                      Lastname2: "",
                      Carnet: "",
                      skillsId: [],
                  };
              }
          },
          function (error) {
              console.log("DEBUG", error);
              $scope.errorRegister = true;
              $scope.search = {
                  Name: "",
                  Lastname1: "",
                  Lastname2: "",
                  Carnet: "",
                  skillsId: [],
              };
          }
    );
    };




};

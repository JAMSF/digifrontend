angular
.module('app')
.controller('lookUserCtrl', lookUserCtrl)
lookUserCtrl.$inject = ['$scope','UserSrv','$state','$rootScope','$stateParams','PostsSrv','$state','DigiStorage','$filter'];

function lookUserCtrl ($scope,UserSrv,$state,$rootScope,$stateParams,PostsSrv,$state,DigiStorage,$filter) {

  /**Validate rol just tutors can see this view **/
  if(!DigiStorage.chekPermissions(2)){
    console.log("DEBUG: ","Not authorized");
    $state.go("appSimple.404");
  }
  /**
   * Fetch data from server about the user info.
   */
  UserSrv.getMyUserinfo($stateParams.userId).then(
    function (response){
      console.log("DEBUG", "USER", response);
      $scope.user = response;
    },
    function (error){
      //$state.go("appSimple.login");

      console.log("DEBUG", "ERROR", error);
    }
  );
  /*check if the user is suscribe to the user*/
  PostsSrv.checkSubscription($stateParams.userId,DigiStorage.getLastUserLogged()).then(
    function (response){
      console.log("DEBUG","ok checking",response);
      if (response == 1){
        /**
         * Fetch the post from server.
         */
        PostsSrv.getMyPosts($stateParams.userId).then(
          function(response){
            $scope.posts = response;
          },
          function(error){
            //$state.go("appSimple.500");
            console.log("DEBUG", "ERROR", error);
          }
        );
      }
      else{
        $scope.alertSubscription = true;
      }
    },
    function (error){
      console.log("DEBUG","Error checking",error);
    }
  );
  /*suscribe to the user*/
  $scope.subscribe = function (){

    console.log("DEBUG", "subscribe", $stateParams.userId, DigiStorage.getLastUserLogged());
    PostsSrv.subscription($stateParams.userId,DigiStorage.getLastUserLogged()).then(
      function (response){
        console.log("DEBUG","Ok subscribe ",response);
        $state.reload();
      },
      function (error){
        console.log("DEBUG","ERROR subscribe ",error);
      }
    );
  };
  /* go back */
  $scope.cancel = function (){
    console.log("DEBUG", "subscribe", $stateParams.userId, DigiStorage.getLastUserLogged());

  };
  /*un suscribe*/

  /*Support */
  $scope.support = function(skill){
    console.log("DEBUG", "SKILL", skill);
    var userId = DigiStorage.getLastUserLogged();
    PostsSrv.support(skill,DigiStorage.getLastUserLogged()).then(
        function (response){
          console.log("DEBUG", "support", response);
          $state.reload();
        },
        function (error){

        }
    );
  };
};

angular
.module('app')
.controller('LoginCtlr', LoginCtlr)
LoginCtlr.$inject = ['$scope','UserSrv','$state','$rootScope','DigiStorage'];

function LoginCtlr ($scope,UserSrv,$state,$rootScope,DigiStorage) {
  
  //close sesión:
  var previousUser = DigiStorage.getLastUserLogged();
  if (previousUser){
    console.log("Closing Session: User ID",previousUser);
    //call to service
  }


  //clear session storage data.
  console.log("Clean session data");
  DigiStorage.clearSessionData();
  //variable to store the user data
  //$scope.password = "";
  //$scope.nickname = "";
  $scope.user = {
    nickname : "",
    password : ""
  };
  /**
    Logs an user.
  **/
  $scope.logUser = function (){
    //get the user name.
    $scope.user = {
      carnet : $scope.nickname,
      password : $scope.password
    };
    $scope.errorCredencial = false;
    UserSrv.logUser($scope.user).then(
      function(response){
        console.log("DEBUG cTRL",response);
        if(response.status == 200){
          //check if is tutor
          if(response.rol == 2){
            $state.go('app.main',{'tutorId':response.id});
          }else{
            $state.go('admin.home',{'adminId':response.id})
          }
          //back up data in session storage
          DigiStorage.saveLoggedUserInfo(response);
        }else{
          $scope.errorCredencial = true;
          $scope.user = {
            nickname : "",
            password : ""
          };
        }
      },
      function(error){
        console.log("DEBUG",error);
        $scope.errorCredencial = true;
        $scope.user = {
          nickname : "",
          password : ""
        };
      }
    );
  };

  /**
      Register an user
  **/
  $scope.register =function(){
    $state.go('appSimple.register');
  };
};

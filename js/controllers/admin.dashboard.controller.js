﻿angular
.module('app')
.controller('AdminCtrl', AdminCtrl)
AdminCtrl.$inject = ['$scope', 'CollegeSrv', 'AbilitieSrv', 'ReportSrv', '$state', 'DigiStorage','UserSrv','$stateParams','toastr'];

function AdminCtrl($scope, CollegeSrv, AbilitieSrv, ReportSrv, $state, DigiStorage,UserSrv,$stateParams,toastr) {


  

    //variable to store the report data
    if(!DigiStorage.chekPermissions(1)){
      console.log("DEBUG: ","Not authorized");
      $state.go("appSimple.404");
    }

    //get the data form user service
    /**
     * Fetch data from server about the user info.
     */
    UserSrv.getMyUserinfo($stateParams.adminId).then(
      function (response){
        console.log("DEBUG", "USER", response);
        $scope.user = response;
      },
      function (error){
        $state.go("appSimple.login");
        console.log("DEBUG", "ERROR", error);
      }
    );
    //
    $scope.collegeId = null;
    $scope.colleges = [];
    $scope.paises = ["Costa Rica", "Estados Unidos", "España", "Alemania", "Argentina", "Brasil", "Canadá", "Chile", "Colombia", "Guatemala", "México", "Panamá"];
    //get the colleges
    CollegeSrv.getColleges().then(
      function (data) {
          //console.log("DEBUG", data);
          $scope.colleges = angular.fromJson(data);
          //console.log("DEBUG", $scope.colleges);
      },
      function (error) {
          console.log("DEBUG", error);
      }
    );

    $scope.abilityId1 = null;
    $scope.abilityId2 = null;
    $scope.abilityId3 = null;
    $scope.abilityId4 = null;
    $scope.abilities = null;

    //get the Abilities
    AbilitieSrv.getAbilities().then(
      function (data) {
          //console.log("DEBUG", data);
          $scope.abilities = angular.fromJson(data);
          //console.log("DEBUG", $scope.colleges);
      },
      function (error) {
          console.log("DEBUG", error);
      }
    );


    /*
    Create a report
    */
    $scope.peso1 = null;
    $scope.peso2 = null;
    $scope.peso3 = null;
    $scope.peso4 = null;

    $scope.skill1 = null;
    $scope.skill2 = null;
    $scope.skill3 = null;
    $scope.skill4 = null;

    $scope.weight1 = null;
    $scope.weight2 = null;
    $scope.weight3 = null;
    $scope.weight4 = null;

    $scope.iduniversidad=null;


    $scope.report = ({
        paiss: "",
        college: 0, //Es opcional se rellena con 0
        habilidadesId: [],
        peso: [], //Se ponen sólo las habilidades que ingresan, no rellenar con 0
        numResultados: ""
    });

    $scope.generateReport = function () {

        if ($scope.numResultados == null) {
            $scope.numResultados = -1;
        }

        if ($scope.universidad == null) {

        if ($scope.peso1 == null && typeof $scope.abilityId1 === "undefined") {
            $scope.weight1 = -1;
            $scope.skill1 = -1;
        }
        else {
            $scope.weight1 = $scope.peso1;
            $scope.skill1 = $scope.abilityId1.IdHabilidad;
        }
        if ($scope.peso2 == null && typeof $scope.abilityId2 === "undefined") {
            $scope.weight2 = -1;
            $scope.skill2 = -1;
        }
        else {
            $scope.weight2 = $scope.peso2;
            $scope.skill2 = $scope.abilityId2.IdHabilidad;
        }
        if ($scope.peso3 == null && typeof $scope.abilityId3 === "undefined") {
            $scope.weight3 = -1;
            $scope.skill3 = -1;
        }
        else {
            $scope.weight3 = $scope.peso3;
            $scope.skill3 = $scope.abilityId3.IdHabilidad;
        }
        if ($scope.peso4 == null && typeof $scope.abilityId4 === "undefined") {
            $scope.weight4 = -1;
            $scope.skill4 = -1;
        }
        else {
            $scope.weight4 = $scope.peso4;
            $scope.skill4 = $scope.abilityId4.IdHabilidad;
        }
        $scope.report = ({
            country: $scope.paiss,
            college: 0, //Es opcional se rellena con 0
            skillsId: [$scope.skill1, $scope.skill2, $scope.skill3, $scope.skill4],
            weights: [$scope.weight1, $scope.weight2, $scope.weight3, $scope.weight4], //Se ponen sólo las habilidades que ingresan, no rellenar con 0
            top: $scope.numResultados,
        });

        }
        else {
            $scope.report = ({
                country: $scope.paiss,
                college: $scope.universidad.IdUniversidad, //Es opcional se rellena con 0
                skillsId: [$scope.skill1, $scope.skill2, $scope.skill3, $scope.skill4],
                weights: [$scope.weight1, $scope.weight2, $scope.weight3, $scope.weight4], //Se ponen sólo las habilidades que ingresan, no rellenar con 0
                top: $scope.numResultados,
            });
        }
  
        $scope.errorRegister = false;
        ReportSrv.createReport($scope.report).then(
          function (response) {
              console.log("DEBUG cTRL", response);
              if (response.status == 200) {
                  $scope.listaTutores = []; // get data from json

                 
                  console.log("Tama;o de response", response.data.length);
                  var datos = JSON.parse(response.data);
                  for (var i = 0; i < datos.length; i++) {

                      //var datosSkills =datos.skills.toString();
                      console.log("DEBUG Skills", datos[i].skills);

                      tutor = {
                          position:i+1,
                          name: datos[i].name,
                          mobilePhone : datos[i].mobilePhone,
                          homePhone : datos[i].homePhone,
                          email : datos[i].email,
                          email2 : datos[i].email2,
                          college : datos[i].college,
                          followers: datos[i].followers,
                          publications: datos[i].publications,
                          reputation: datos[i].reputation,
                          skills: datos[i].skills,
                          score : datos[i].score,
                      };

                      $scope.listaTutores.push(tutor);
                  
                  }

              } else {
                  $scope.errorRegister = true;
                  $scope.report = ({
                      paiss: 0,
                      college: "", //Es opcional se rellena con 0
                      habilidadesId: [],
                      peso: [], //Se ponen sólo las habilidades que ingresan, no rellenar con 0
                      numResultados: "",
                  });
              }
          },
          function (error) {
              console.log("DEBUG", error);
              $scope.errorRegister = true;
              $scope.report = ({
                  paiss: 0,
                  college: "", //Es opcional se rellena con 0
                  habilidadesId: [],
                  peso: [], //Se ponen sólo las habilidades que ingresan, no rellenar con 0
                  numResultados: "",
              });
          }
        );
    }


  



};

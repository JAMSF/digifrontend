angular
.module('app')
.config(['$stateProvider', '$urlRouterProvider', '$ocLazyLoadProvider', '$breadcrumbProvider', function ($stateProvider, $urlRouterProvider, $ocLazyLoadProvider, $breadcrumbProvider) {

    $urlRouterProvider.otherwise('/login');

    $ocLazyLoadProvider.config({
        // Set to true if you want to see what and when is dynamically loaded
        debug: true
    });

    $breadcrumbProvider.setOptions({
        prefixStateName: 'app.main',
        includeAbstract: true,
        template: '<li class="breadcrumb-item" ng-repeat="step in steps" ng-class="{active: $last}" ng-switch="$last || !!step.abstract"><a ng-switch-when="false" href="{{step.ncyBreadcrumbLink}}">{{step.ncyBreadcrumbLabel}}</a><span ng-switch-when="true">{{step.ncyBreadcrumbLabel}}</span></li>'
    });

    $stateProvider
    .state('app', {
        abstract: true,
        templateUrl: 'views/common/layouts/full.html',
        //page title goes here
        ncyBreadcrumb: {
            label: 'Root',
            skip: true
        },
        resolve: {
            loadCSS: ['$ocLazyLoad', function ($ocLazyLoad) {
                // you can lazy load CSS files
                return $ocLazyLoad.load([{
                    serie: true,
                    name: 'Font Awesome',
                    files: ['css/font-awesome.min.css']
                }, {
                    serie: true,
                    name: 'Simple Line Icons',
                    files: ['css/simple-line-icons.css']
                }]);
            }],
            loadPlugin: ['$ocLazyLoad', function ($ocLazyLoad) {
                // you can lazy load files for an existing module
                return $ocLazyLoad.load([{
                    serie: true,
                    name: 'chart.js',
                    files: [
                      'bower_components/chart.js/dist/Chart.min.js',
                      'bower_components/angular-chart.js/dist/angular-chart.min.js'
                    ]
                }]);
            }],
        }
    })
    .state('app.main', {
        url: '/dashboard/:tutorId',
        templateUrl: 'views/main.html',
        //page title goes here
        ncyBreadcrumb: {
            label: 'Mis publicaciones',
        },
        controller: 'TutorMainPost'
    })
    .state('appSimple', {
        abstract: true,
        templateUrl: 'views/common/layouts/simple.html',
        resolve: {
            loadPlugin: ['$ocLazyLoad', function ($ocLazyLoad) {
                // you can lazy load files for an existing module
                return $ocLazyLoad.load([{
                    serie: true,
                    name: 'Font Awesome',
                    files: ['css/font-awesome.min.css']
                }, {
                    serie: true,
                    name: 'Simple Line Icons',
                    files: ['css/simple-line-icons.css']
                }]);
            }],
        }
    })
    //Search States
    .state('app.search', {
         url: '/search/:userId',
         templateUrl: 'views/search.html',
         //page title goes here
         ncyBreadcrumb: {
             label: 'Busqueda miembros de la comunidad',
         },
         controller: 'SearchCtrl'
     })
     //look my Posts
     .state('app.myposts', {
          url: '/myposts/:userId',
          templateUrl: 'views/myposts.html',
          //page title goes here
          ncyBreadcrumb: {
              label: 'Mis publicaciones',
          },
          controller: 'MyPostsCtrl'
      })
    //look user
    .state('app.lookUser', {
         url: '/user/:userId',
         templateUrl: 'views/lookUser.html',
         //page title goes here
         ncyBreadcrumb: {
             label: 'Consultar usuario',
         },
         controller: 'lookUserCtrl'
     })
    //Admin States
    .state('admin', {
        abstract: true,
        templateUrl: 'views/common/layouts/adminbase.html',
        //page title goes here
        ncyBreadcrumb: {
            label: 'admin',
            skip: true
        },
        resolve: {
            loadCSS: ['$ocLazyLoad', function ($ocLazyLoad) {
                // you can lazy load CSS files
                return $ocLazyLoad.load([{
                    serie: true,
                    name: 'Font Awesome',
                    files: ['css/font-awesome.min.css']
                }, {
                    serie: true,
                    name: 'Simple Line Icons',
                    files: ['css/simple-line-icons.css']
                }]);
            }]
        }
    })

    .state('admin.home', {
          url: '/admin/:adminId',
          templateUrl: 'views/adminmain.html',
          //page title goes here
          ncyBreadcrumb: {
              label: 'Mi panel de administrador',
          },
          controller: 'AdminCtrl'
      })

    // Additional Pages
    .state('appSimple.login', {
        url: '/login',
        templateUrl: 'views/pages/login.html',
        controller: 'LoginCtlr'
    })
    .state('appSimple.register', {
        url: '/register',
        templateUrl: 'views/pages/register.html',
        controller: 'RegisterCtlr'
    })
    .state('appSimple.404', {
        url: '/404',
        templateUrl: 'views/pages/404.html'
    })
    .state('appSimple.500', {
        url: '/500',
        templateUrl: 'views/pages/500.html'
    })
}]);
